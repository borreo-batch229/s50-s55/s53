import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { Link, NavLink } from "react-router-dom";

export default function AppNavbar() {
   return (
      <Navbar expand="md" className="text-light p-2 m-1">
         <Navbar.Brand as={NavLink} exact to="/">
            Zuitt
         </Navbar.Brand>

         <Navbar.Toggle aria-controls="basic-navbar-nav" />
         <Navbar.Collapse id="basic-navbar-bav">
            <Nav className="ml-auto">
               <Nav.Link as={NavLink} exact to="/">
                  Home
               </Nav.Link>
               <Nav.Link as={NavLink} exact to="/courses">
                  Courses
               </Nav.Link>
               <Nav.Link as={NavLink} exact to="/login">
                  Login
               </Nav.Link>
               <Nav.Link as={NavLink} exact to="/register">
                  Register
               </Nav.Link>
            </Nav>
         </Navbar.Collapse>
      </Navbar>
   );
}
