import Banner from "../components/Banner.js";
import Highlights from "../components/Highlights.js";

export default function Home() {
   return (
      <>
         <Banner
            heading="Zuitt Coding Bootcamp"
            paragraph="Opportunities for everyone, everywhere!"
            state="Enroll Now!"
         />
         <Highlights />
      </>
   );
}
